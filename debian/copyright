Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OpenLP
Upstream-Contact: Raoul Snyman <raoul@snyman.info>
Source: https://openlp.org/

Files: *
Copyright: 2004-2017 OpenLP Developers, https://openlp.org/
License: GPL-2

Files: tests/resources/*
Copyright: Public Domain
License: public-domain
 Christian Hymns in the Public Domain

Files: openlp/plugins/remotes/html/jquery.js
       openlp/plugins/remotes/html/jquery.min.js
       openlp/plugins/remotes/html/jquery.mobile.js
       openlp/plugins/remotes/html/jquery.mobile.min.js
       openlp/plugins/remotes/html/jquery.mobile.min.css
       openlp/plugins/remotes/html/jquery-migrate.js
       openlp/plugins/remotes/html/jquery-migrate.min.js
Copyright: jQuery Foundation and other contributors,
           https://jquery.org/
License: Expat or GPL-2

Files: openlp/core/ui/media/vendor/vlc.py
Copyright: 2009-2012 the VideoLAN team
License: LGPL-2.1+

Files: resources/images/*.png
Copyright: 2007 Nuno Pinheiro <nuno@oxygen-icons.org>
           2007 David Vignoni <david@icon-king.com>
           2007 David Miller <miller@oxygen-icons.org>
           2007 Johann Ollivier Lapeyre <johann@oxygen-icons.org>
           2007 Kenneth Wimer <kwwii@bootsplash.org>
           2007 Riccardo Iaconelli <riccardo@oxygen-icons.org>
License: LGPL-2

Files: resources/images/README.txt
       resources/images/about-new.bmp
       resources/images/android_app_qr.png
       resources/images/openlp*
       resources/images/OpenLP.ico
       resources/images/projector_connect.png
       resources/images/projector_cooldown.png
       resources/images/projector_error.png
       resources/images/projector_hdmi.png
       resources/images/projector_manager.png
       resources/images/projector_off.png
       resources/images/projector_on.png
       resources/images/projector_spacer.png
       resources/images/projector_warmup.png
       resources/images/wizard_createtheme.bmp
       resources/images/wizard_exportsong.bmp
       resources/images/wizard_firsttime.bmp
       resources/images/wizard_importbible.bmp
       resources/images/wizard_importsong.bmp
Copyright: 2004-2017 OpenLP Developers, https://openlp.org/
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; version
 2 of the License only.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2
 On Debian GNU/Linux systems, the complete text of the GNU Library General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License along
 with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in ‘/usr/share/common-licenses/LGPL-2.1’.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
